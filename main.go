package main

import (
	"bitbucket.org/vithubati/event-overlap/pkg/event"
	"encoding/json"
	"fmt"
	"log"
	"time"
)

func getEvents() []event.Event {
	return []event.Event{
		{
			Start: time.Date(2021, time.Month(2), 21, 8, 00, 30, 0, time.UTC),
			End:   time.Date(2021, time.Month(2), 21, 10, 00, 30, 0, time.UTC),
		},
		{
			Start: time.Date(2021, time.Month(2), 21, 8, 00, 30, 0, time.UTC),
			End:   time.Date(2021, time.Month(2), 21, 10, 00, 30, 0, time.UTC),
		},
		{
			Start: time.Date(2021, time.Month(2), 21, 11, 00, 30, 0, time.UTC),
			End:   time.Date(2021, time.Month(2), 21, 12, 00, 30, 0, time.UTC),
		},
		{
			Start: time.Date(2021, time.Month(2), 21, 9, 00, 30, 0, time.UTC),
			End:   time.Date(2021, time.Month(2), 21, 9, 40, 30, 0, time.UTC),
		},
		{
			Start: time.Date(2021, time.Month(2), 21, 13, 00, 30, 0, time.UTC),
			End:   time.Date(2021, time.Month(2), 21, 14, 00, 30, 0, time.UTC),
		},
		{
			Start: time.Date(2021, time.Month(2), 21, 14, 00, 30, 0, time.UTC),
			End:   time.Date(2021, time.Month(2), 21, 16, 00, 30, 0, time.UTC),
		},
	}
}

func main() {
	events := getEvents()
	jsE, err := json.Marshal(events)
	if err != nil {
		log.Fatalf("unmarshaling error %v", err)
	}
	fmt.Println(fmt.Sprintf("Input data: %s", string(jsE)))
	overlaps := event.FindOverlappingEvents(events)
	jsO, err := json.Marshal(overlaps)
	if err != nil {
		log.Fatalf("unmarshaling error %v", err)
	}
	fmt.Println(fmt.Sprintf("Output data: %s", string(jsO)))
}
