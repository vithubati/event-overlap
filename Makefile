
.PHONY: test
test:
	go test ./pkg/... --cover

.PHONY: run
run:
	go run ./main.go