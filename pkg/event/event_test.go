package event

import (
	"fmt"
	"reflect"
	"testing"
	"time"
)

func Test_findOverlappingEvents(t *testing.T) {
	type args struct {
		events []Event
	}
	tests := []struct {
		name string
		args args
		want []Event
	}{
		{
			name: "Test_success_scenerio",
			args: args{events: []Event{
				{
					Start: time.Date(2021, time.Month(2), 21, 8, 00, 30, 0, time.UTC),
					End:   time.Date(2021, time.Month(2), 21, 10, 00, 30, 0, time.UTC),
				},
				{
					Start: time.Date(2021, time.Month(2), 21, 8, 00, 30, 0, time.UTC),
					End:   time.Date(2021, time.Month(2), 21, 10, 00, 30, 0, time.UTC),
				},
				{
					Start: time.Date(2021, time.Month(2), 21, 11, 00, 30, 0, time.UTC),
					End:   time.Date(2021, time.Month(2), 21, 12, 00, 30, 0, time.UTC),
				},
				{
					Start: time.Date(2021, time.Month(2), 21, 9, 00, 30, 0, time.UTC),
					End:   time.Date(2021, time.Month(2), 21, 9, 40, 30, 0, time.UTC),
				},
				{
					Start: time.Date(2021, time.Month(2), 21, 13, 00, 30, 0, time.UTC),
					End:   time.Date(2021, time.Month(2), 21, 14, 00, 30, 0, time.UTC),
				},
				{
					Start: time.Date(2021, time.Month(2), 21, 14, 00, 30, 0, time.UTC),
					End:   time.Date(2021, time.Month(2), 21, 16, 00, 30, 0, time.UTC),
				},
			}},
			want: []Event{
				{
					Start: time.Date(2021, time.Month(2), 21, 8, 00, 30, 0, time.UTC),
					End:   time.Date(2021, time.Month(2), 21, 10, 00, 30, 0, time.UTC),
				},
				{
					Start: time.Date(2021, time.Month(2), 21, 8, 00, 30, 0, time.UTC),
					End:   time.Date(2021, time.Month(2), 21, 10, 00, 30, 0, time.UTC),
				},
				{
					Start: time.Date(2021, time.Month(2), 21, 9, 00, 30, 0, time.UTC),
					End:   time.Date(2021, time.Month(2), 21, 9, 40, 30, 0, time.UTC),
				},
			},
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			got := FindOverlappingEvents(tt.args.events)
			fmt.Println(fmt.Sprintf("overlaps events are %+v", got))
			if !reflect.DeepEqual(got, tt.want) {
				t.Errorf("want %+v got %+v", tt.want, got)
			}
		})
	}
}
