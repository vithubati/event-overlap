package event

import "time"

type Event struct {
	Start time.Time
	End   time.Time
}

// FindOverlappingEvents - finds the overlapping []Event within the given []Event
// The complexity is O(n^2)
func FindOverlappingEvents(events []Event) []Event {
	length := len(events)
	overlaps := make([]Event, 0, length)
	for i := 0; i < len(events); i++ {
		for j := 0; j < len(events); j++ {
			if i == j {
				continue
			}
			if earliest(events[i].Start, events[j].Start).Before(latest(events[i].End, events[j].End)) {
				overlaps = append(overlaps, events[i])
				break
			}
		}
	}
	return overlaps
}

// earliest - it will compare the given dates and return the earliest date
func earliest(startE1, startE2 time.Time) time.Time {
	if startE2.After(startE1) {
		return startE2
	}
	return startE1
}

// latest - it will compare the given dates and return the latest date
func latest(startE1, startE2 time.Time) time.Time {
	if startE2.Before(startE1) {
		return startE2
	}
	return startE1
}
